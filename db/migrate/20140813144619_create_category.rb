class CreateCategory < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleContent::Category.table_name do |t|
      t.string :name
      t.string :slug
      t.timestamps
    end
 
    create_table Refinery::FlexibleContent::Categorization.table_name do |t|
      t.integer :category_id
      t.integer :project_id
      t.timestamps
    end
  end
end
