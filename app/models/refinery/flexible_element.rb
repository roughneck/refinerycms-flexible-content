class Refinery::FlexibleElement < Refinery::Core::BaseModel
  belongs_to :page
  belongs_to :project
  belongs_to :journal
  belongs_to :flex_element, polymorphic: true, dependent: :destroy

  accepts_nested_attributes_for :flex_element

  # For now describe flexible content object in here
  ELEMENT_TYPES = [
      {
          title: 'Text',
          class: 'Refinery::FlexibleContent::Text'
      },
      {
          title: 'Image',
          class: 'Refinery::FlexibleContent::ImageElement'

      },
      # {
      #     title: 'ImageText',
      #     class: 'Refinery::FlexibleContent::ImageText',
      # },
      # {
      #     title: 'TextThreeColumn',
      #     class: 'Refinery::FlexibleContent::TextThreeColumn'
      # },
      {
          title: 'ImageList',
          class: 'Refinery::FlexibleContent::ImageList'
      }
  ]

  def build_flex_element(params)
    raise "Unknown FlexibleContentType: #{flex_element_type}" unless ELEMENT_TYPES.map { |element| element[:class] }.include?(flex_element_type)
    self.flex_element = flex_element_type.constantize.new(params)
  end

end