class Refinery::FlexibleContent::ImageText < Refinery::Core::BaseModel
  has_one :flexible_element, class_name: 'Refinery::FlexibleElement', as: :flex_element
  belongs_to :image, class_name: ::Refinery::Image, foreign_key: :image_id

  def image_positions
    I18n.t('refinery.admin.image_texts.image_positions').map { |key, value| [value, key.to_s] }
  end

end