class CreateFlexibleElements < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleElement.table_name do |t|
      t.references :flex_element, polymorphic: true
      t.integer :page_id
      t.integer :position
      t.timestamps
    end

    create_table Refinery::FlexibleContent::ImageText.table_name do |t|

    end

    add_index :refinery_flexible_elements, :page_id
  end
end
