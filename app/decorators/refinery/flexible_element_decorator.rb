class Refinery::FlexibleElementDecorator < ::Draper::Decorator
  delegate_all

  def render_form(f)
    base_path = '/refinery/admin/'
    object_type = flex_element_type.split('::').last.underscore
    f.fields_for :flex_element, f.object.flex_element do |builder|
      h.render "#{base_path}#{object_type.pluralize}/form", f: builder
    end
  end

  def render_content
    base_path = '/refinery/'
    h.render "#{base_path}#{element_type.underscore.pluralize}/#{element_type.underscore}", element: flex_element
  end

  def element_type
    flex_element_type.split('::').last
  end
end