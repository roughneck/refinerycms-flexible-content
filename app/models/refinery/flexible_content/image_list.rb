class Refinery::FlexibleContent::ImageList < Refinery::Core::BaseModel
  has_one :flexible_element, class_name: 'Refinery::FlexibleElement', as: :flex_element
  has_many :image_list_items, class_name: 'Refinery::FlexibleContent::ImageListItem'
  accepts_nested_attributes_for :image_list_items, allow_destroy: true

  def style_options
  	I18n.t('refinery.admin.image_lists.style_options').map { |key, value| [value, key.to_s] }
  end

  def shuffle?
  	self.style == 'shuffle'
  end
end