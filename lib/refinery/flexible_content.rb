#require 'refinerycms-core'

Gem.loaded_specs['refinerycms-flexible-content'].dependencies.each do |d|
  require d.name
end

module Refinery
  autoload :FlexibleContentGenerator, 'generators/refinery/flexible_content_generator'

  module FlexibleContent

    class << self
      def root
        @root ||= Pathname.new(File.expand_path('../../../', __FILE__))
      end

      def factory_paths
        @factory_paths ||= [root.join('spec', 'factories').to_s]
      end

      def attach!
        require 'refinery/page'
        require 'refinery/flexible_content/extend_models'

        config_object_sanitizer(config.enabled_models) do |model_class|
          model_class.send :has_many_flexible_elements
        end

        config_object_sanitizer(config.enabled_controllers) do |controller_class|
          object_type = controller_class.to_s.split('::').last.underscore.humanize.split(' ').first.singularize.downcase.to_sym
          controller_class.class_eval do
            alias_method "orig_#{object_type}_params".to_sym, "#{object_type}_params".to_sym

            define_method("#{object_type}_params") do
              flex_hash = {
                  flexible_elements_attributes: params[object_type.to_sym].include?(:flexible_elements_attributes) ? params.require(object_type.to_sym).require(:flexible_elements_attributes).permit! : nil
              }.reject { |k, v| v.nil? }
              orig_params = send("orig_#{object_type}_params")
              orig_params.merge(flex_hash)
            end
          end
        end
      end

      private
      def config_object_sanitizer(config_object_collection, &block)
        config_object_collection.each do |config_object|
          unless (safe_object = config_object.safe_constantize)
            Rails.logger.warn "FlexibleContent is unable to find class: #{config_object}"
            next
          end
          block.call(safe_object)
        end
      end

    end
    require 'refinery/flexible_content/engine'
  end
end