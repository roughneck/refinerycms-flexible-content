require 'refinery/flexible_content/configuration'

module Refinery
  module FlexibleContent
    class Engine < ::Rails::Engine
      include Refinery::Engine

      isolate_namespace Refinery
      engine_name :refinery_flexible_content

      before_inclusion do
        Refinery::Plugin.register do |plugin|
          plugin.name = 'flexible_content'
          plugin.pathname = root
          plugin.hide_from_menu = true
        end
      end

      config.to_prepare do
        Refinery::FlexibleContent.attach!
      end

      config.after_initialize do
        Refinery.register_engine(Refinery::FlexibleContent)
      end
    end
  end
end