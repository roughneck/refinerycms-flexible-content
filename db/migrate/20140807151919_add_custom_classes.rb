class AddCustomClasses < ActiveRecord::Migration
  def change
    add_column :refinery_image_texts, :custom_class, :string
    add_column :refinery_texts, :custom_class, :string
  end
end
