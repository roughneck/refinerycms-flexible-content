$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'refinery/flexible_content/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name = 'refinerycms-flexible-content'
  s.version = Refinery::FlexibleContent::VERSION
  s.authors = ['Nicolai Schulten']
  s.email = ['n.schulten@dashochhaus.de']
  s.homepage = 'http://dashochhaus.de'
  s.summary = 'TODO: Summary of RefinerycmsFlexibleContent.'
  s.description = 'TODO: Description of RefinerycmsFlexibleContent.'
  s.license = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.rdoc']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'rails', '=> 4.1.4'
  s.add_dependency 'refinerycms-pages', '~> 3.0.0.dev'
  s.add_dependency 'draper', '~> 1.3.0'

end