class AddCustomClassToTextThreeColumn < ActiveRecord::Migration
  def change
  	add_column :refinery_text_three_columns, :custom_class, :string
  end
end
