class Refinery::FlexibleContent::ImageTextDecorator < Draper::Decorator
  delegate_all

  def translated_image_position
    I18n.t(image_position, :scope => 'refinery.admin.image_texts.image_position')
  end

  def render_element
    render_text.concat(render_image)
  end

  def render_text
    overlay = full_size && !is_background_image? ? ' overlay' : ''
    h.content_tag :div, class: 'inner-grid' do
      h.content_tag :div, class: "flexible-text flex-position-#{is_background_image? || full_size ? text_position + overlay : text_position_opposite}" do
        h.raw body
      end
    end
  end

  def div_descriptor
    css = %w'flexible-element-block flexible-image-text-element'
    css << custom_class
    if full_size
      css << 'full-size'
    end
    h.raw("class='#{css.join(' ')}'").concat(background)
  end

  def background
    style = []
    if is_background_image?
      style << "background: url(#{image.url}) no-repeat;"
      if full_size
        style << "background-size: cover;"
      end
    end
    h.raw "style='#{style.join(' ')}'"
  end

  # Render image with given constraint
  def render_image

    image_url = lambda {
      if (image_width || image_height) && !full_size
        geometry_string = "#{image_width ? image_width : ''}x#{image_height ? image_height : ''}"
        h.image_tag image.thumbnail(geometry: geometry_string).url
      else
        h.image_tag image.url
      end
    }

    if image && !is_background_image? && !full_size
      (h.content_tag(:div, class: "flexible-image flex-position-#{image_position}") do
        image_url.call
      end) +
      (h.content_tag(:div, class: 'image-caption') do
        caption.html_safe
      end)
    elsif image && !is_background_image?
      image_url.call
    end
  end

  def text_position_opposite
    return 'left' if image_position == 'right'
    return 'right' if image_position == 'left'
    return 'above' if image_position == 'below'
    return 'below' if image_position == 'above'
  end
end