class CreateTextElement < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleContent::TextThreeColumn.table_name do |t|
      t.text :body_1
      t.text :body_2
      t.text :body_3
      t.timestamps
    end
  end
end
