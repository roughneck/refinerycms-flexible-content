class CreateImageListItem < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleContent::ImageListItem.table_name do |t|
    	t.text :text
    	t.integer :image_id
    	t.integer :image_list_id
    	t.string :custom_class
    end
  end
end
