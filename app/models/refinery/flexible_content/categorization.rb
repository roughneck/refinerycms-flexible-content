class Refinery::FlexibleContent::Categorization < Refinery::Core::BaseModel
  belongs_to :category
  belongs_to :project
end