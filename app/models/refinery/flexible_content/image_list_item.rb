class Refinery::FlexibleContent::ImageListItem < Refinery::Core::BaseModel
  belongs_to :image_list, class_name: 'Refinery::FlexibleElement::ImageList', foreign_key: :image_list_id
  belongs_to :image, class_name: ::Refinery::Image, foreign_key: :image_id
end