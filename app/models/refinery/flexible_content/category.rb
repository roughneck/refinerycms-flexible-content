class Refinery::FlexibleContent::Category < Refinery::Core::BaseModel
  has_many :categorizations
  has_many :projects , through: :categorizations
end