class AddImages < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleContent::ImageElement.table_name do |t|
      t.integer :image_id
      t.integer :image_width
      t.integer :image_height
      t.string :custom_class
      t.string :caption
      t.timestamps
    end
  end
end
