module Refinery
  module FlexibleContent
    module ExtendModels
      def has_many_flexible_elements
        has_many :flexible_elements, proc { order('position ASC') }
        accepts_nested_attributes_for :flexible_elements, allow_destroy: true
      end
    end
  end
end


ActiveRecord::Base.send(:extend, Refinery::FlexibleContent::ExtendModels)