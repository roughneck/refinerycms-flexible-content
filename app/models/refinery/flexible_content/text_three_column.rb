class Refinery::FlexibleContent::TextThreeColumn < Refinery::Core::BaseModel
  has_one :flexible_element, class_name: 'Refinery::FlexibleElement', as: :flex_element
end