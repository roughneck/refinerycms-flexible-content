module Refinery
  module FlexibleContent
    include ActiveSupport::Configurable

    config_accessor :enable_for

    self.enable_for = [
        {:model => 'Refinery::Page', :tab => 'Refinery::Pages::Tab', controller: 'Refinery::Admin::PagesController'},
    ]

    if defined?(Refinery::Projects)
      self.enable_for << {model: 'Refinery::Projects::Project', controller: 'Refinery::Projects::Admin::ProjectsController', tab: 'Refinery::Project::Tab'}
    end

    if defined?(Refinery::Journals)
      self.enable_for << {model: 'Refinery::Journals::Journal', controller: 'Refinery::Journals::Admin::JournalsController', tab: 'Refinery::Journals::Tab'}
    end

    config.instance_eval do
      # def enabled_tabs
      #   extract_enabled_option(:tab)
      # end

      def enabled_controllers
        extract_enabled_option(:controller)
      end

      def enabled_models
        extract_enabled_option(:model)
      end

      private
      def extract_enabled_option(key)
        enable_for.map { |enable_for| enable_for[key] }.compact
      end
    end

  end
end