class AddIsBackgroundImageToImageText < ActiveRecord::Migration
  def change
  	add_column :refinery_image_texts, :is_background_image, :boolean
  end
end
