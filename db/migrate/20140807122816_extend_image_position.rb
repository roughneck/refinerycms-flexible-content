class ExtendImagePosition < ActiveRecord::Migration
  def change
    change_column :refinery_image_texts, :image_position, :string
    add_column :refinery_image_texts, :image_width, :integer
    add_column :refinery_image_texts, :image_height, :integer
  end
end
