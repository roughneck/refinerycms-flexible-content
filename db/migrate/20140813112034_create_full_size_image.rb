class CreateFullSizeImage < ActiveRecord::Migration
  def change
    create_table :refinery_full_size_images do |t|
    	t.text :text
    	t.integer :image_id
    	t.string :custom_class
    end
  end
end
