class AddCaptionsToImages < ActiveRecord::Migration
  def change
    add_column :refinery_image_list_items, :caption, :string
    add_column :refinery_image_texts, :caption, :string
  end
end
