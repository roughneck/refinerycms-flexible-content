class AddTextModule < ActiveRecord::Migration
  def change
    create_table Refinery::FlexibleContent::Text.table_name do |t|
      t.text :body
      t.timestamps
    end
  end
end
