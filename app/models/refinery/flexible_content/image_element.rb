class Refinery::FlexibleContent::ImageElement < Refinery::Core::BaseModel
  has_one :flexible_element, class_name: 'Refinery::FlexibleElement', as: :flex_element
  belongs_to :image, class_name: ::Refinery::Image, foreign_key: :image_id

end