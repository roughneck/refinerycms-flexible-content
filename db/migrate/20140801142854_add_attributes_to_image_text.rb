class AddAttributesToImageText < ActiveRecord::Migration
  def change
    add_column Refinery::FlexibleContent::ImageText.table_name, :image_position, :integer, default: 0
    add_column Refinery::FlexibleContent::ImageText.table_name, :body, :text
    add_column Refinery::FlexibleElement::ImageText.table_name, :image_id, :integer
  end
end
