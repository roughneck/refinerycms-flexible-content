$(function () {
    (function () {
        // FlexibleContent JS
        $(document).on('click', '.remove_element', function (e) {
            var $element = $(e.currentTarget);
            $element.parent().find('input.destroy').val(true);
            $element.parent().hide();
        })

        $(document).on('click', '.add_flexible_content', function (e) {
            e.preventDefault();
            var time = new Date().getTime(),
                regexp = new RegExp($('#flex_element_type option:selected').data('id'), 'g'),
                elementsOnPage = $('#flexible_elements fieldset').length,
                $new_element = $('#flex_element_type option:selected');
            $('#flexible_elements').append($new_element.data('form').replace(regexp, time));
            $('#flexible-elements fieldset').last().find('input.position').val(elementsOnPage);
            reposition();
            init_modal_dialogs();

//            if ($("#submit_continue_button").length) {
//                $("#submit_continue_button").trigger('click');
//                location.reload();
//            }
        });
        $(document).on('click', '.flexible-elements-positioner a', function (e) {
            e.preventDefault();
            var $target = $(e.currentTarget),
                $container = $target.closest('fieldset'),
                $elementsContainer = $('#flexible_elements'),
                $elmentsContainerList = $elementsContainer.find('fieldset');
            var index = $elmentsContainerList.index($container);

            if ($target.hasClass('up-end')) {
                $elementsContainer.prepend($container);
            } else if ($target.hasClass('down-end')) {
                $elementsContainer.append($container);
            } else if ($target.hasClass('up')) {
                if (index > 0) {
                    $($elmentsContainerList[index - 1]).before($container);
                }
            } else {
                if (index < $elmentsContainerList.length) {
                    $($elmentsContainerList[index + 1]).after($container);
                }
            }
            reposition();
        });

        function reposition() {
            $('#flexible_elements fieldset').each(function (index, element) {
                $(element).find('input.position').val(index);
            })
        }

        $('form').on('click', '.add_item', function (event) {
            time = new Date().getTime();
            regexp = new RegExp($(this).data('id'), 'g');
            $(this).before($(this).data('fields').replace(regexp, time));
            init_modal_dialogs();
            event.preventDefault();
        });

        $('form').on('click', '.remove_item', function (e) {
            var $element = $(e.currentTarget).parent();
            $element.parent().find('input[type=hidden]').val(true);
            $element.parent().hide();
        })

        $(document).on('click', '.flexible-elements h3', function () {
            $(this).closest('.flexible-elements').toggleClass('open');
        });

        $('#submit_continue_preview_button').insertBefore('.form-actions .form-actions-left #cancel_button').css({'margin-right':'5px'});


//        $("#content").on("DOMSubtreeModified", function(){
//            $(document).find('img').each(function(){
//                console.log($(this).attr('src'));
//                console.log($(this).attr('src').replace(/sha=.+(\?.+)/,''));
//            });
//        });
    })();

});
