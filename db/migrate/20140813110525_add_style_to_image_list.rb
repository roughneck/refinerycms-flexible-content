class AddStyleToImageList < ActiveRecord::Migration
  def change
  	add_column :refinery_image_lists, :style, :string
  end
end
