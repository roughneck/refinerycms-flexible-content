class AddFullSizeImageToImage < ActiveRecord::Migration
  def up
    drop_table :refinery_full_size_images
    change_table :refinery_image_texts do |t|
      t.boolean :full_size, default: false
      t.string :text_position
    end
  end

  def down
    change_table :refinery_image_texts do |t|
      t.remove :full_size
      t.remove :text_position
    end
    create_table :refinery_full_size_images do |t|
      t.string :text
      t.integer :image_id
      t.string :custom_class
      t.timestamps
    end
  end
end
