#encoding=utf-8
module Refinery
  module FlexibleContent
    module FormHelper
      def self.link_to_add_flexible_elements(view, f, association)
        options = []
        Refinery::FlexibleElement::ELEMENT_TYPES.each do |element|
          new_object = f.object.send(association).klass.new
          new_object.flex_element = (element[:class].constantize.new)
          id = new_object.flex_element.object_id
          f.fields_for(association, new_object, child_index: id) do |fbuilder|
              options << [
                  element[:title],
                  element[:class],
                  data: {
                      form: "#{view.render('/refinery/admin/flexible_elements', f: fbuilder)}",
                      id: id
                  }
              ]
          end
        end
        view.select_tag(
            :flex_element_type,
            view.options_for_select(options)
        )
      end

      def self.link_to_add_image_list_items(view, f, association)
        new_object = f.object.send(association).klass.new
        id = new_object.object_id
        fields = f.fields_for(association, new_object, child_index: id) do |builder|
          view.render('/refinery/admin/image_list_items', f: builder)
        end

        view.link_to('Item hinzufügen', '#', class: "add_item", data: {id: id, fields: fields.gsub("\n", "")})
      end
    end
  end
end